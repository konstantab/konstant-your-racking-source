Canada’s largest supplier of new pallet racking, used racking, industrial shelving, warehouse automation systems, and storage systems equipment. We also do rack inspections, pallet rack repair, rack installations, and take downs, as well as warehouse design and engineering.

Address: 5345 54 St SE, Calgary, AB T2C 4R3, CAN

Phone: 866-733-4725

Website: https://www.konstant.com/en/contact-us/canada/calgary-alberta
